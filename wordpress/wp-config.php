<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'test-apprenteam' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'd=};|.gJ?mR?cH1@ep7CcvQo.6^.dT-(u>w={o@5Qs85`Y&&~xSY:<Z5Umy we=B' );
define( 'SECURE_AUTH_KEY',  'VzZm_ .z|RqKQ^A:D}PI]w+0fbl=r$`a7w f+_C,7O{oP*{sCj` 4{<c+!2SWEQh' );
define( 'LOGGED_IN_KEY',    'O[@b$I5C<,T.yG$A?hR) M%(x7$s4 J RZ&qzq,CxJD#Gh3cP]?P%cRrW>]}x^k:' );
define( 'NONCE_KEY',        '?Z5y ]xfD U8[P>Y;&yuF,!x/>2wzEgG(Qs8SR<0{7B`lGS&L:lN7cF[hxw5;vq9' );
define( 'AUTH_SALT',        'Z1Vxpd/!@FW3nvxG_(Ie~+.g5/G`] VQtt@,d_2VdL}aj8`7 w]$=_LfZKI]0gzc' );
define( 'SECURE_AUTH_SALT', 'yN*aPg-PA*8gOJ3cwG{9>eHP.u3e}nq&$6oJkGJvTw8EmJAicQVUjLOAn#Z;SgE*' );
define( 'LOGGED_IN_SALT',   'S4Efed85Sj~_Z._XhbhB:<Y6u8npN2)7rd$R.0}|6NlN}M>{?O|OLr&h;^5je9SY' );
define( 'NONCE_SALT',       'Hjx8RD#4-LoS-CHIH3sf?byT!q =Zz7?Eh.1o?}]_C$DdmS3d@DgcsQa9w5#(u8M' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_test_app_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
